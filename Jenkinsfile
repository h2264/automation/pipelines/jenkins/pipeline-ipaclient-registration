pipeline {
  agent {
    node {
      label 'drone'
    }
  }

  environment {
    IPA = credentials('internal_ipa_credelntials')
    FQDN = "${HOSTNAME}.${DOMAIN}"
  }

  options {
    ansiColor('gnome-terminal')
    buildDiscarder(logRotator(daysToKeepStr: '7', numToKeepStr: '20'))
  }

  parameters {
    string(
        name: 'HOSTNAME',
        defaultValue: '',
        description: 'VM (proxmox) hostname'
    )
    string(
        name: 'HOST_IP',
        defaultValue: '0.0.0.0',
        description: 'VM (proxmox) ipv4.'
    )
    string(
        name: 'DOMAIN',
        defaultValue: 'fwc.dc.ops.vm', 
        description: 'VM (proxmox) domain'
    )
    string(
        name: '_SLACK_CHANNEL',
        defaultValue: 'jenkins-pipeline-freeipa-client-registration',
        description: 'Slack channel to receive notifications'
    )
    credentials credentialType: 
        'com.datapipe.jenkins.vault.credentials.common.VaultSSHUserPrivateKeyImpl', 
        defaultValue: 'internal_bootstrapp_sshkeypair',
        name: 'BOOTSTRAPP_CREDS',
        required: true
    booleanParam(
        name: 'IPA_GETAPPCERT',
        defaultValue: false,
        description: 'Request application cert'
    )
    choice choices:
        ['ipaclient-registration.yaml'],
        name: 'PLAYBOOK'
  }

  stages {

    // ToDo: Make this dynamic.
    //  Read from the filesystem, populate playbook list
    stage('Create List') {
      steps {
          script {
              taglist = ["prereq","install","register"]
          }
      }
    }

    stage('Dynamic Stages') {
      environment {
        EXTRAVARS="-e target_hostname='${params.HOSTNAME}' -e target_domain='${params.DOMAIN}' -e app_cert='${params.IPA_GETAPPCERT}' -e target_ip='${params.HOST_IP}' -e ipa_usr='${IPA_USR}' -e ipa_psw='${IPA_PSW}'"
      }
      steps {
        script {
          for(int i=0; i < taglist.size(); i++) {
            stage("TAG: ${taglist[i]}"){
              ansiblePlaybook become: true, 
                colorized: true, 
                credentialsId: "${params.BOOTSTRAPP_CREDS.toString()}", 
                disableHostKeyChecking: true,
                extras: EXTRAVARS,
                installation: 'virtualenv',
                inventory: "${HOST_IP},",
                playbook: params.PLAYBOOK,
                tags: taglist[i]
            }
          }
        }
      }
    }
  }

  post {
    always {
      script {
        BUILD_USER = getBuildUser()
        COLOR_MAP = [
          'SUCCESS': 'good',
          'FAILURE': 'danger',
        ]
      }
      slackSend channel: params._SLACK_CHANNEL,
          color: COLOR_MAP[currentBuild.currentResult],
          message: """
          *${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}
          GIT_URL: ${env.GIT_URL}
          GIT_BRANCH: ${env.GIT_BRANCH}
          GIT_COMMIT: ${env.GIT_COMMIT}
          More info at: ${env.BUILD_URL}
          """
    }
    cleanup {
        cleanWs()
    }
  }
}

String getBuildUser() {
  ID = currentBuild.getBuildCauses('hudson.model.Cause$UserIdCause').userId
  if (ID) {
    return ID
  }
  else {
    return '[cronTrigger: */5 * * * *]'
  }
}