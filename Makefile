.PHONY: venv_init all precheck download build clean

_VENV=.venv
_EVARS=-e ansible_ssh_private_key_file=~/.ssh/customkeys/pve
_INVENTORY=./inventory.yaml
_PLAYBOOK=ipaclient-registration.yaml

all: prereq download build clean
	
prereq:
	$(call venv_exec,$(_VENV),ansible-playbook -i $(_INVENTORY) $(_PAYLOAD) $(_EVARS) --tags=prereq $(_PLAYBOOK))

download:
	$(call venv_exec,$(_VENV),ansible-playbook -i $(_INVENTORY) $(_PAYLOAD) $(_EVARS) --tags=download $(_PLAYBOOK))

build:
	$(call venv_exec,$(_VENV),ansible-playbook -i $(_INVENTORY) $(_PAYLOAD) $(_EVARS) --tags=build $(_PLAYBOOK))

clean:
	$(call venv_exec,$(_VENV),ansible-playbook -i $(_INVENTORY) $(_PAYLOAD) $(_EVARS) --tags=cleanup $(_PLAYBOOK))

test:
	$(call venv_exec,$(_VENV),ansible-playbook -i $(_TESTIP), $(_PLAYBOOK))

# VENV FUNCTIONS
define venv_exec
	$(if [ ! -f "$($(1)/bin/activate)" ], python3 -m venv $(1))
	( \
    	source $(1)/bin/activate; \
    	$(2) \
	)
endef
