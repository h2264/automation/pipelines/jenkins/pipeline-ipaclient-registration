# Jenkins pipeline ipaclient registration

Declarative Jenkins pipeline responsible for the following:
- Update and upgrade host binaries through the host package manager (apt, yum)
- Installation of FreeIPA client binaries
- Configuration of FreeIPA client and registration to the FreeIPA server
- Configuration for automated SSL certificate management

### Parameters
| Parameter | Description |
| --- | --- |
| `HOSTNAME` | Name of host |
| `HOST_IP` | IPv4 |
| `DOMAIN` | Subnet domain name |
| `REALM` | FreeIPA realm |
| `BOOTSTRAPP_CREDS` | SSH credentials used for bootstrapping VM's |
| `IPA` | FreeIPA credentials with host registration access |
| `IPASERVER` | FreeIPA server `$FQDN` |
| `CERTPATH` | Absolute path used to store certs and keys |
|||

### Steps
| Step | Description |
| --- | --- |
| Declarative: Checkout SCM | Pull this repository to refresh Pipeline code |
| Update and Upgrade System | Update/Upgrade using the package manager |
| Install FreeIPA client | Install FreeIPA client binaries using the package manager |
| Register node to FreeIPA server | Configuration of FreeIPA client and registration to the FreeIPA server |
| Request FreeIPA certificates | Configuration for automated SSL certificate management |
|||